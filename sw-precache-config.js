/* eslint-env node */

module.exports = {
  staticFileGlobs: [
    'bower_components/webcomponentsjs/webcomponents-loader.js',
    'manifest.json',
  ],
  runtimeCaching: [
    {
      urlPattern: /\/bower_components\/webcomponentsjs\/.*.js/,
      handler: 'fastest',
      options: {
        cache: {
          name: 'webcomponentsjs-polyfills-cache',
        },
      },
    },
  ],
  /**
   * Avoid service worker navigate fallback on /_ urls
   * see: https://github.com/Polymer/polymer-cli/issues/290
   */
  navigateFallback: '/index.html',
  navigateFallbackWhitelist: [ /^\/[^\_]+\/?/ ],
};
